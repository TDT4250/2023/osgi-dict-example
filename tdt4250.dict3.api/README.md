# tdt4250.dict3.api bundle

This bundle is part of the modular and flexible variant 3 of the [tdt4250.dict project](../README.md), and was created using the API template. It contains both the core interfaces and classes needed by those that consume or implement dictionaries, but it does not itself provide/export any components.

## Packages

- **tdt4250.dict3.api**: The core interfaces and classes that will be used by dictionary users and implementations. There's a **Dict** interface, a **DictSearch** class managing search within a set of **Dict**s and a **DictSearchResult** class for the search result.
