package tdt4250.dict3.it;

import org.osgi.service.component.annotations.Component;

import tdt4250.dict3.api.Dict;
import tdt4250.dict3.util.WordsDict;

@Component(
		property = {
				WordsDict.DICT_NAME_PROP + "=it",
				WordsDict.DICT_RESOURCE_PROP + "=tdt4250.dict3.it#/tdt4250/dict3/it/it.txt"}
		)
public class ItDict extends WordsDict implements Dict {
}
